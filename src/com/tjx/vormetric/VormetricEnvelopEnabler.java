package com.tjx.vormetric;

import static sun.security.pkcs11.wrapper.PKCS11Constants.CKM_AES_CBC_PAD;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStore.Entry;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import atg.core.util.Base64;
import sun.security.pkcs11.wrapper.CK_MECHANISM;

public class VormetricEnvelopEnabler {
	
	private static final int min_param_count = 4;
	private static String libpath = "/opt/vormetric/DataSecurityExpert/agent/pkcs11/lib/libvorpkcs11.so";
	public static enum ENV {
		QA,
		PROD
	}
	public static enum BANNER {
		TJMAXX,
		MARSHALLS,
		HOMEGOODS
	}
	public static enum ACTION {
		createKeyStore,
		importKey,
		assertKeys,
		deleteKeys,
		deleteKeyStore,
		testAlgo
		
	}
	private static Map<String, String> pwd_map  = new HashMap<String, String>() {{
	    put(ENV.QA+"_"+BANNER.TJMAXX, "qnAwNlmH0Adg!");
	    put(ENV.PROD+"_"+BANNER.TJMAXX, "bEt6wRY2dF3I!");
	    put(ENV.QA+"_"+BANNER.MARSHALLS, "qnAwNlmH0Adg!");
	    put(ENV.PROD+"_"+BANNER.MARSHALLS, "bEt6wRY2dF3I!");
	    put(ENV.QA+"_"+BANNER.HOMEGOODS, "qnAwNlmH0Adg!");
	    put(ENV.PROD+"_"+BANNER.HOMEGOODS, "bEt6wRY2dF3I!");
	}};
	public static final String ENCRYPTION_AES = "AES";
	public static final String ENCRYPTION_AES_CBC = "AES/CBC/PKCS5Padding";
	public static final String ENCRYPTION_AES_ECB = "AES/ECB/PKCS5Padding";
	
	public static final byte[] iv = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};
	private static String pin = null;
	private static String keyStorePwd = null;
	private static String vormetricLibPath = null;
	private static Boolean debugMode = Boolean.FALSE;
	public static void main(String[] args) {
		
		 System.out.println("Start Vormetrics VTE Enabler task.");
		 String overridePin = null;
		 String overRideKeyStorePwd = null;
		 String overRideLibPath = null;
		
		 ACTION action = null;
		 String keyStorePath = null;
		 String keyListStr = null;
		 ENV env = null;
		 BANNER banner = null;
		 List<String> keyAliasList = null;
		 
		 
		 try 
		 {
			 // read the params
		        if (args.length < min_param_count) {
		            usage();
		            System.exit(0);
		        }
		        for (int i = 0; i < args.length; i += 2) {
		            if      (args[i].equals("-p")) overridePin = args[i + 1];
		            else if (args[i].equals("-action")) action = ACTION.valueOf(args[i + 1]);
		            else if (args[i].equals("-path")) keyStorePath = args[i + 1];
		            else if (args[i].equals("-storePwd")) overRideKeyStorePwd = args[i + 1];
		            else if (args[i].equals("-keys")) keyListStr = args[i + 1];
		            else if (args[i].equals("-libPath")) overRideLibPath = args[i + 1];
		            else if (args[i].equals("-env")) env = ENV.valueOf(args[i + 1]);
		            else if (args[i].equals("-banner")) banner = BANNER.valueOf(args[i + 1]);
		            else if (args[i].equals("-debug")) debugMode = Boolean.parseBoolean(args[i + 1]);
		           
		            else usage();
		        }
		        
		        applyOverRides(overridePin,overRideKeyStorePwd,overRideLibPath,banner,env);
		        System.out.println("Action selected : "+action);
		        switch(action)
		        {
		        	case createKeyStore :
		        		System.out.println("Creating KeyStores at "+keyStorePath);
		        		createKeyStore(keyStorePath,keyStorePwd);
		        		System.out.println("Done Creating KeyStores at "+keyStorePath);
		        		break;
		        	case importKey :
		        		System.out.println("Importing Keys into KeyStore at "+keyStorePath);
		        		keyAliasList = Stream.of(keyListStr.split(",", -1)).collect(Collectors.toList());
		        		importKeys(keyStorePath,keyStorePwd,keyAliasList,pin);
		        		System.out.println("Done Importing Keys into KeyStore at "+keyStorePath);
		        		break;
		        	case assertKeys :
		        		System.out.println("Printing Keys");
		        		keyAliasList = Stream.of(keyListStr.split(",", -1)).collect(Collectors.toList());
		        		printKeys(keyStorePath,keyStorePwd,keyAliasList);
		        		System.out.println("Done Printing Keys");
		        		break;
		        	case deleteKeys :
		        		System.out.println("Deleting Keys");
		        		keyAliasList = Stream.of(keyListStr.split(",", -1)).collect(Collectors.toList());
		        		DeleteKeys(keyStorePath,keyStorePwd,keyAliasList);
		        		System.out.println("Done Deleting Keys");
		        		break;
		        	case deleteKeyStore :
		        		System.out.println("Deleting KeyStore");
		        		DeleteKeyStore(keyStorePath);
		        		System.out.println("Done Deleting KeyStore");
		        		break;
		        	case testAlgo :
		        		System.out.println("Testing Algorithms");
		        		keyAliasList = Stream.of(keyListStr.split(",", -1)).collect(Collectors.toList());
		        		TestAlgo(keyStorePath,keyStorePwd,keyAliasList);
		        		System.out.println("Done Testing Algorithms");
		        		break;
		        	default :
		        		throw new Exception("Invalid Operation");
		        }
		 }
		 catch(Exception e )
		 {
			 e.printStackTrace();
		 }
		 System.out.println("End Vormetrics VTE Enabler task.");
	}
	
	private static void applyOverRides(String overridePin, String overRideKeyStorePwd, String overRideLibPath, BANNER banner, ENV env) {
		
		if(null != overridePin)
		{
			pin = overridePin;
		}
		else {
			pin = pwd_map.get(env.toString()+"_"+banner.toString());
		}
		
		// keystore password
		if(null != overRideKeyStorePwd)
		{
			keyStorePwd = overRideKeyStorePwd;
		}
		else {
			keyStorePwd = pwd_map.get(env.toString()+"_"+banner.toString());
		}
		// lib path
		if(null != overRideLibPath)
		{
			vormetricLibPath = overRideLibPath;
		}
		else
		{
			vormetricLibPath = libpath;
		}
		if(debugMode)
		{
			System.out.println("ENV is "+env);
			System.out.println("Banner is "+banner);
			System.out.println("pin is "+pin);
			System.out.println("keyStorePwd is "+keyStorePwd);
			System.out.println("vormetricLibPath is "+vormetricLibPath);
		}
	}

	private static void TestAlgo(String keyStorePath, String keyStorePwd, List<String> keyAliasList) {
		if(null != keyStorePath && null != keyStorePwd && null != keyAliasList) 
		{
			
			try {
				
				Cipher mEncryptCypher = null;
				Cipher cbcEncryptor = null;
				Cipher ecbEncryptor = null;
				
				
				char[] pwdArray = keyStorePwd.toCharArray();
				
				
				KeyStore ks = KeyStore.getInstance("JCEKS");
				ks.load(new FileInputStream(keyStorePath), pwdArray);
				for(String keyName: keyAliasList)
				{
					System.out.println("Key Store contains key : "+keyName+" Result :"+ks.containsAlias(keyName));
				}
				try {
					for(String keyName: keyAliasList)
					{
						KeyStore.ProtectionParameter keyPwd = new KeyStore.PasswordProtection("".toCharArray());
						Entry entry = ks.getEntry(keyName, keyPwd);
						if(entry != null)
						{
							SecretKey key = (SecretKey) ks.getKey(keyName, "".toCharArray());
							System.out.println("Key Algorithm is - "+key.getAlgorithm());
							System.out.println("Key format is - "+key.getFormat());
							System.out.println("Key length is - "+key.getEncoded().length*8);
							
							// Testing different alog modes
							String simpleText = "ABCD";
							
							
							System.out.println("Simple Text is "+simpleText);
							
							mEncryptCypher = Cipher.getInstance(ENCRYPTION_AES);
							mEncryptCypher.init(Cipher.ENCRYPT_MODE, key);
							byte[] encSimpleValue = mEncryptCypher.doFinal(simpleText.getBytes());
							String encryptedAESSimpleText = Base64.encodeToString(encSimpleValue);
							System.out.println("Encrypted using "+ENCRYPTION_AES+" value is - "+encryptedAESSimpleText);
							
							cbcEncryptor = Cipher.getInstance(ENCRYPTION_AES_CBC);
							cbcEncryptor.init(Cipher.ENCRYPT_MODE, key);
							byte[] encCBCValue = cbcEncryptor.doFinal(simpleText.getBytes());
							String encryptedCBCSimpleText = Base64.encodeToString(encCBCValue);
							System.out.println("Encrypted using "+ENCRYPTION_AES_CBC+" value is - "+encryptedCBCSimpleText);
							
							ecbEncryptor = Cipher.getInstance(ENCRYPTION_AES_ECB);
							ecbEncryptor.init(Cipher.ENCRYPT_MODE, key);
							byte[] encECBValue = ecbEncryptor.doFinal(simpleText.getBytes());
							String encryptedECBSimpleText = Base64.encodeToString(encECBValue);
							System.out.println("Encrypted using "+ENCRYPTION_AES_ECB+" value is - "+encryptedECBSimpleText);
							
							
							// end of algo mode testing
						}
					}
				} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnrecoverableEntryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalBlockSizeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (BadPaddingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchPaddingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvalidKeyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		else
		{
			System.out.println("Not enough params to perform operation");
		}
	}

	private static void DeleteKeyStore(String keyStorePath) {
		
		try {
			Files.delete(Paths.get(keyStorePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private static void DeleteKeys(String keyStorePath, String keyStorePwd, List<String> keyAliasList) {
		
		if(null != keyStorePath && null != keyStorePwd && null != keyAliasList)
		{
			try {
			
			// loading the keyStore
			char[] pwdArray = keyStorePwd.toCharArray();
			KeyStore ks = KeyStore.getInstance("JCEKS");
			ks.load(new FileInputStream(keyStorePath), pwdArray);
			
				for (String keyName : keyAliasList) {
					
					System.out.println("Key Store contains key : "+keyName+" Result :"+ks.containsAlias(keyName));
					System.out.println("Trying to delete key : " + keyName);
					ks.deleteEntry(keyName);
	    			System.out.println("END : delete the key");
	    			}
			FileOutputStream fos = new FileOutputStream(keyStorePath);
   			ks.store(fos, pwdArray); 
    			
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
		
	}

	private static void importKeys(String keyStorePath, String keyStorePwd, List<String> keyAliasList, String pin) {
		
		if(null != keyStorePath && null != keyStorePwd && null != keyAliasList && null != pin)
		{
			Vpkcs11Session session = null;
			try {
			
			// loading the keyStore
			char[] pwdArray = keyStorePwd.toCharArray();
			KeyStore ks = KeyStore.getInstance("JCEKS");
			ks.load(new FileInputStream(keyStorePath), pwdArray);
			
			// session creation
			
			
				session = Helper.startUp(Helper.getPKCS11LibPath(libpath), pin);
				for (String keyName : keyAliasList) {
					
					
					System.out.println("Trying to import key : " + keyName);
					System.out.println("Trying to find the key in session...");
				       
	                long keyHandle = Helper.findKey(session, keyName) ;
	                byte[] keyBytes = null;
	                SecretKey key = null;
	                System.out.println("keyHandle is - "+keyHandle);
	    			if (keyHandle != 0)
	    			{
	    				System.out.println("Trying to extract key");
	    			    
	    			    CK_MECHANISM mechanism = new CK_MECHANISM (CKM_AES_CBC_PAD, iv);
	    				//passing in an invalid wrapping key handle
	    			    keyBytes = session.p11.C_WrapKey(session.sessionHandle, mechanism, 0, keyHandle );
	    			    if(null != keyBytes)
	    			    {
	    			    	System.out.println("key bytes isnt null");
	    			    	key = new SecretKeySpec(keyBytes, 0, keyBytes.length, "AES"); 
	    			    }
	    			    System.out.println("END - Trying to extract key");
	    			    System.out.println("Storing the key");
	    			    
	    			    KeyStore.SecretKeyEntry entry = new KeyStore.SecretKeyEntry(key);
	    			    
	    			    KeyStore.ProtectionParameter keyPwd = new KeyStore.PasswordProtection("".toCharArray());
	    			   ks.setEntry(keyName, entry, keyPwd);
	    			   FileOutputStream fos = new FileOutputStream(keyStorePath);
	   				   ks.store(fos, pwdArray); 
	    			   System.out.println("END : Storing the key");
	    			}
	    			else
	    			{
	    			    // shouldn't happen.
	    				System.out.println("Source Key not found. No exporting");
	    			}
	    			
	    			
	    			
				} 
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally {
				Helper.closeDown(session);
			}
			
			
		}
		
	}

	private static void printKeys(String keyStorePath, String keyStorePwd, List<String> keyAliasList) {
		
		if(null != keyStorePath && null != keyStorePwd && null != keyAliasList) 
		{
			
			try {
				char[] pwdArray = keyStorePwd.toCharArray();
				
				
				KeyStore ks = KeyStore.getInstance("JCEKS");
				ks.load(new FileInputStream(keyStorePath), pwdArray);
				for(String keyName: keyAliasList)
				{
					System.out.println("Key Store contains key : "+keyName+" Result :"+ks.containsAlias(keyName));
				}
				try {
					for(String keyName: keyAliasList)
					{
						KeyStore.ProtectionParameter keyPwd = new KeyStore.PasswordProtection("".toCharArray());
						Entry entry = ks.getEntry(keyName, keyPwd);
						if(entry != null)
						{
							SecretKey key = (SecretKey) ks.getKey(keyName, "".toCharArray());
							System.out.println("Key Algorithm is - "+key.getAlgorithm());
							System.out.println("Key format is - "+key.getFormat());
							System.out.println("Key length is - "+key.getEncoded().length*8);
						}
					}
				} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnrecoverableEntryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		else
		{
			System.out.print("Not enough params to perform operation");
		}
	}

	private static void createKeyStore(String keyStorePath, String keyStorePwd) {
		
		if(null != keyStorePath && null != keyStorePwd)
		{
			try {
				File keyStoreFile = new File(keyStorePath);
				if(!keyStoreFile.exists())
				{
					keyStoreFile.createNewFile();
				}
				KeyStore ks = KeyStore.getInstance("JCEKS");
				System.out.println("Default keyStore type "+KeyStore.getDefaultType());
				System.out.println("keyStore type "+ks.getType());
				char[] pwdArray = keyStorePwd.toCharArray();
				ks.load(null, pwdArray);
				FileOutputStream fos = new FileOutputStream(keyStorePath);
				ks.store(fos, pwdArray); 
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			System.out.println("You need path and password to create a key store");
		}
		
	}

	static void usage()
    {
        System.out.println("usage: java com.tjx.vormetric.VormetricEnvelopEnabler -banner [TJMAXX | MARSHALLS | HOMEGOODS] -env [QA | PROD] -action [createKeyStore | importKey | assertKeys | deleteKeys | deleteKeyStore | testAlgo] -path <keystore-path>  [-p pin] [-storePwd <sotre-pwd>] -keys <comma-seperated-names> [-libPath <vormetric-lib-path>] [-debug true]");
        System.exit (1);
    }
	
	
}
