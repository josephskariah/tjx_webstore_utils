package com.tjx.test;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;

import javax.crypto.AEADBadTagException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import atg.core.util.Base64;

public class TestEncryption {
	
	public static final String ENCRYPTION_AES = "AES";
	public static final String ENCRYPTION_AES_CBC = "AES/CBC/PKCS5Padding";
	public static final String ENCRYPTION_AES_ECB = "AES/ECB/PKCS5Padding";
	
	
	public static final String ENCRYPTION_GCM = "AES/GCM/NoPadding";
	
	private static transient Cipher mEncryptCypher = null;
	private static transient Cipher mDecryptCypher = null;
	
	private static transient Cipher mEncryptGCMCypher = null;
	private static transient Cipher mDecryptGCMCypher = null;
	
	private static transient Cipher cbcEncryptor = null;
	private static transient Cipher ecbEncryptor = null;
	
	public static final int GCM_IV_LENGTH = 12;
    public static final int GCM_TAG_LENGTH = 16;
	

	public static void main(String[] args) {
	
		System.out.println("Testing Start");
		
		// Params to be stored.
		String storePassword = "password";
		String storeType="JCEKS";
		String provider="SunJCE";
		String storeLocation = "/Users/jskariah/projects/Midnight/temp/dev-keystore.jks";
		String keyAlias = "aes-pci-256";
		
		KeyStore keyStore = null;
		char[] keyStorePassword = storePassword.toCharArray();
		SecretKey key = null;
		
		// text to be encrypted
		String text = "This is a plain text verrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrry long";
		
        for (int i = 0; i < args.length; i += 2) {
            if      (args[i].equals("-path")) storeLocation = args[i + 1];
            if      (args[i].equals("-storePassword")) storePassword = args[i + 1];
            if      (args[i].equals("-keyAlias")) keyAlias = args[i + 1];
            
        }
		
		//loading the local key store
		try {
			keyStore = KeyStore.getInstance(storeType, provider);
			File f = new File(storeLocation);
			
			if (f != null && f.exists()) {
				FileInputStream fis = null;
				try {
					fis = new FileInputStream(f);
					keyStore.load(fis, keyStorePassword);
				} finally {
					if (fis != null) {
						fis.close();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// retriving the key from keystore
		if(null != keyStore)
		{
			try {
				key = (SecretKey) keyStore.getKey(keyAlias, keyStorePassword);
				System.out.println("Key Algorithm is - "+key.getAlgorithm());
				System.out.println("Key format is - "+key.getFormat());
				System.out.println("Key length is - "+key.getEncoded().length*8);
			} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// testing the encryption and decryption
		if(null != key)
		{
			try {
				// initing AES
				mEncryptCypher = Cipher.getInstance(ENCRYPTION_AES);
				mEncryptCypher.init(Cipher.ENCRYPT_MODE, key);
				mDecryptCypher = Cipher.getInstance(ENCRYPTION_AES);
				mDecryptCypher.init(Cipher.DECRYPT_MODE, key);
				
				
				// Testing different alog modes
				String simpleText = "ABCD";
				
				
				System.out.println("Simple Text is "+simpleText);
				
				byte[] encSimpleValue = mEncryptCypher.doFinal(simpleText.getBytes());
				String encryptedAESSimpleText = Base64.encodeToString(encSimpleValue);
				System.out.println("Encrypted using "+ENCRYPTION_AES+" value is - "+encryptedAESSimpleText);
				
				cbcEncryptor = Cipher.getInstance(ENCRYPTION_AES_CBC);
				cbcEncryptor.init(Cipher.ENCRYPT_MODE, key);
				
				byte[] encCBCValue = cbcEncryptor.doFinal(simpleText.getBytes());
				String encryptedCBCSimpleText = Base64.encodeToString(encCBCValue);
				System.out.println("Encrypted using "+ENCRYPTION_AES_CBC+" value is - "+encryptedCBCSimpleText);
				
				ecbEncryptor = Cipher.getInstance(ENCRYPTION_AES_ECB);
				ecbEncryptor.init(Cipher.ENCRYPT_MODE, key);
				
				byte[] encECBValue = ecbEncryptor.doFinal(simpleText.getBytes());
				String encryptedECBSimpleText = Base64.encodeToString(encECBValue);
				System.out.println("Encrypted using "+ENCRYPTION_AES_ECB+" value is - "+encryptedECBSimpleText);
				
				
				// end of algo mode testing
				
				System.out.println("Cipher Algorithm "+mEncryptCypher.getAlgorithm());
				System.out.println("Cipher Method "+mEncryptCypher.getInstance("AES"));
				
				
				//GCM
				
				byte[] IV = new byte[GCM_IV_LENGTH];
		        SecureRandom random = new SecureRandom();
		        random.nextBytes(IV);
		        
		        SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
		        // Create GCMParameterSpec
		        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV);
		        
				mEncryptGCMCypher = Cipher.getInstance(ENCRYPTION_GCM);
				mEncryptGCMCypher.init(Cipher.ENCRYPT_MODE, key, gcmParameterSpec);
				mDecryptGCMCypher = Cipher.getInstance(ENCRYPTION_GCM);
				mDecryptGCMCypher.init(Cipher.DECRYPT_MODE, key,gcmParameterSpec);

				//actual work - Exercise #1 - Encrypt and decrypt using AES - the old way
				System.out.println("#######################################");
				System.out.println("Start - Encrypt and decrypt using AES");
				byte[] encValue = mEncryptCypher.doFinal(text.getBytes());
				String encryptedAESStr = Base64.encodeToString(encValue);
				
				System.out.println("Encrypted using "+ENCRYPTION_AES+" value is - "+encryptedAESStr);
				
				
				byte[] encryptArray = Base64.decodeToByteArray(encryptedAESStr);
				
				String decryptedStr = new String(mDecryptCypher.doFinal(encryptArray));
				System.out.println("Decrypted using "+ENCRYPTION_AES+" value is - "+decryptedStr);
				System.out.println("Done - Encrypt and decrypt using AES");
				
				
				System.out.println("#######################################");
				
				// Exercise #2 - Trying to decrypt using GCM the encrypted text using AES. Exception Expected.
				System.out.println("Start - Decrypt using GCM/AES and encryt using GCM");
				String decryptUsingGCM = decryptWithGCMorAES(encryptArray);
				System.out.println("Done - Decrypt using GCM/AES");
				
				System.out.println("#######################################");
				
				// Exercise #3 - encrypt and decrypt using GCM
				System.out.println("Start - Encrypt using GCM");
				
				byte[] encValueGCM  = mEncryptGCMCypher.doFinal(text.getBytes());
				String encryptedGCMStr = Base64.encodeToString(encValueGCM);
				
				System.out.println("Encrypted using "+ENCRYPTION_GCM+" value is - "+encryptedGCMStr);
				
				
				//System.out.println("Testing "+new String());
				
				byte[] encryptGCMArray = Base64.decodeToByteArray(encryptedGCMStr);
				
				String decryptedText = decrypt(encryptGCMArray, key, IV);
		        System.out.println("DeCrypted Text: " + decryptedText);
		        mDecryptGCMCypher.init(Cipher.DECRYPT_MODE, key,gcmParameterSpec);
				String decryptValueofGCM = decryptWithGCMorAES(encryptGCMArray);
				System.out.println("Decrypted using "+ENCRYPTION_GCM+" value is - "+decryptValueofGCM);
				
				System.out.println("Done - Encrypt using GCM");
				
				System.out.println("#######################################");
				
				
				// Exercise #4 - Encrypt - persit ... reinit the key and decrypt
				
				System.out.println("Original Text : " + text);
				
				// try #1 - just reinit key
				
				byte[] cipherText;
				try {
					System.out.println("Try #1");
					IV = new byte[GCM_IV_LENGTH];
					random = new SecureRandom();
					random.nextBytes(IV);
					    
					cipherText = encrypt(text.getBytes(), key, IV);
					System.out.println("Encrypted Text : " + Base64.encodeToString(cipherText));
					
					// reseting
					IV = null;
					IV = new byte[GCM_IV_LENGTH];
					random = new SecureRandom();
					random.nextBytes(IV);
					// exception expected as the IVs dont match.
					decryptedText = decrypt(cipherText, key, IV);
					System.out.println("DeCrypted Text : " + decryptedText);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    
			    // try #2 - persist the IV + Cipher and read.
			    
			    try {
			    	System.out.println("Try #2");
					IV = new byte[GCM_IV_LENGTH];
					random = new SecureRandom();
					random.nextBytes(IV);
					    
					cipherText = encrypt(text.getBytes(), key, IV);
					System.out.println("Encrypted Text : " + Base64.encodeToString(cipherText));
					System.out.println("IV : " + new String(IV));
					ByteBuffer bb = ByteBuffer.allocate(4+IV.length+cipherText.length);
					bb.putInt(IV.length);
					bb.put(IV);
					bb.put(cipherText);
					encryptedGCMStr = Base64.encodeToString(bb.array());
					
					IV = null;
					//System.out.println("IV : " + new String(IV));
					byte[] persistedArray = Base64.decodeToByteArray(encryptedGCMStr);
					ByteBuffer decryptBb = ByteBuffer.wrap(persistedArray);
					int ivLength = decryptBb.getInt();
					IV = new byte[ivLength];
					decryptBb.get(IV);
					byte[] dataInPersistedArray = new byte[decryptBb.remaining()];
					decryptBb.get(dataInPersistedArray);
					decryptedText = decrypt(dataInPersistedArray, key, IV);
					System.out.println("DeCrypted Text  : " + decryptedText);
					System.out.println("IV : " + new String(IV));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    
			    
			    System.out.println("****************************************");

				
			} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BadPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidAlgorithmParameterException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Testing End");
		
		// Testing the key length
	       try {
			System.out.println("Generate a 256 bit = 32 byte long AES key");
			    KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
			    keyGenerator.init(256);
			    SecretKey secretKey = keyGenerator.generateKey();
			    System.out.println("secretKey: " + secretKey);
			    // the key is a 32 bytes long byte array
			    byte[] aesKey = secretKey.getEncoded();
			    System.out.println("aesKey length: " + aesKey.length);
			    System.out.println("aesKey:" + aesKey);
			    // convert to a base64 encoded String
			    String aesKeyBase64 = Base64.encodeToString(aesKey);
			    System.out.println("aesKeyBase64: " + aesKeyBase64);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	private static String decryptWithGCMorAES(byte[] encryptArray) {
		String decryptedStr = null;
		try
		{
			try {
				decryptedStr = new String(mDecryptGCMCypher.doFinal(encryptArray));
				if(null != decryptedStr)
				{
					System.out.println("Decrypted using "+ENCRYPTION_GCM+" value is - "+decryptedStr);
				}
			}
			catch(AEADBadTagException e )
			{
				e.printStackTrace();
				System.out.println("Cant decrypt using GCM going to try using AES");
				decryptedStr = new String(mDecryptCypher.doFinal(encryptArray));
				if(null != decryptedStr)
				{
					System.out.println("Decrypted using "+ENCRYPTION_AES+" value is - "+decryptedStr);
				}
			}
			
		}
		catch(Exception e)
		{
			System.out.println("Cant decrypt at all using GCM or AES");
			e.printStackTrace();
		}
		
		return decryptedStr;
	}
	
	 public static byte[] encrypt(byte[] plaintext, SecretKey key, byte[] IV) throws Exception
	    {
	        // Get Cipher Instance
	        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
	        
	        // Create SecretKeySpec
	        SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
	        
	        // Create GCMParameterSpec
	        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV);
	        
	        // Initialize Cipher for ENCRYPT_MODE
	        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);
	        
	        // Perform Encryption
	        byte[] cipherText = cipher.doFinal(plaintext);
	        
	        return cipherText;
	    }

	    public static String decrypt(byte[] cipherText, SecretKey key, byte[] IV) throws Exception
	    {
	        // Get Cipher Instance
	        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
	        
	        // Create SecretKeySpec
	        SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
	        
	        // Create GCMParameterSpec
	        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV);
	        
	        // Initialize Cipher for DECRYPT_MODE
	        cipher.init(Cipher.DECRYPT_MODE, keySpec, gcmParameterSpec);
	        
	        // Perform Decryption
	        byte[] decryptedText = cipher.doFinal(cipherText);
	        
	        return new String(decryptedText);
	    }

}
