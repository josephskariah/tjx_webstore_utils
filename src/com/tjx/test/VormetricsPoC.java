package com.tjx.test;

import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_ALWAYS_SENSITIVE;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_DECRYPT;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_ENCRYPT;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_EXTRACTABLE;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_KEY_TYPE;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_LABEL;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_NEVER_EXTRACTABLE;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_SENSITIVE;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_SIGN;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_TOKEN;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_UNWRAP;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_VALUE_LEN;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_VERIFY;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKA_WRAP;
import static sun.security.pkcs11.wrapper.PKCS11Constants.CKM_AES_CTR;

import java.nio.charset.Charset;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;


import sun.security.pkcs11.wrapper.CK_ATTRIBUTE;
import sun.security.pkcs11.wrapper.CK_MECHANISM;
import sun.security.pkcs11.wrapper.PKCS11Exception;

public class VormetricsPoC {
	private static String libpath = "/opt/vormetric/DataSecurityExpert/agent/pkcs11/lib/libvorpkcs11.so";
	private static String utfMode = "UTF-8";
	private static String plainText = "This is a plain text";
	private static String dekKeyAlias = "";
	private static String kekKeyAlias = null;
	public static final int GCM_IV_LENGTH = 12;
	private static int keyLife = 1;
	
	public static byte[] iv = null;
	
	
	//
    public static final int CKA_THALES_DEFINED = 0x40000000;
    public static final int CKA_THALES_KEY_STATE_START_DATE  = 0x10;

	public static final int CKA_THALES_KEY_STATE_DEACTIVATED = CKA_THALES_DEFINED | 0x1004;
	public static final int CKA_THALES_DATE_OBJECT_CREATE    = CKA_THALES_DEFINED | 0x1B;
	 
	public static final int CKA_THALES_DATE_KEY_DEACTIVATION = CKA_THALES_KEY_STATE_DEACTIVATED | CKA_THALES_KEY_STATE_START_DATE;

    // Attributes for version key creation
	public static final int CKA_THALES_KEY_VERSION_ACTION = 0x40000082;
	public static final int CKA_THALES_KEY_VERSION_LIFE_SPAN = 0x40000083;
	
	public static void main(String[] args) {
		
		 System.out.println("Start Vormetrics POC.");
		 String pin = null;
		 String headerMode = "";
		 long headerEnc = 0;
		 long headerDec = 0;
	     CK_MECHANISM encMech = null;
	     CK_MECHANISM decMech = null;
	     
	     Boolean debugMode = Boolean.FALSE;
	     
	     long dekKeyHandle = 0;
	     long kekKeyHandle = 0;
		 // read the arguments
		 // -p for pin
		 // -master for masterAlias
		 
	        if (args.length < 2) {
	            usage();
	            System.exit(0);
	        }
	        
	        for (int i = 0; i < args.length; i += 2) {
	            if      (args[i].equals("-p")) pin = args[i + 1];
	            else if (args[i].equals("-kek")) kekKeyAlias = args[i + 1];
	            else if (args[i].equals("-dek")) dekKeyAlias = args[i + 1];
	            else if (args[i].equals("-debug")) debugMode = Boolean.parseBoolean(args[i + 1]);
	            else usage();
	        }
	        
	        Vpkcs11Session session = null;
	        // warmup - get session and try to encrypt and decrypt plain text using dek - same IV
	        try {
				if(kekKeyAlias != null && !dekKeyAlias.isEmpty())
				{
	
				    System.out.println("Start EncryptDecryptMessage ...");
				    session = Helper.startUp(Helper.getPKCS11LibPath(libpath), pin);
			        headerEnc = Helper.CKM_THALES_V27HDR | Helper.CKM_VENDOR_DEFINED;
			        headerDec = Helper.CKM_THALES_ALLHDR | Helper.CKM_VENDOR_DEFINED;
			        
			        iv = new byte[GCM_IV_LENGTH];
	                encMech = new CK_MECHANISM(headerEnc | CKM_AES_CTR, iv);
	                decMech = new CK_MECHANISM(headerDec | CKM_AES_CTR, iv);
	                
	                // find the key
	                dekKeyHandle = Helper.findKey(session, dekKeyAlias);
	                if(dekKeyHandle != 0)
	                {
	                	System.out.println("DEK found in vormetrics - Dek Key Handle is "+dekKeyHandle);
	                	printKeyLength(session,dekKeyHandle);
	    				//printing the key attributes of dek - this is for debug purposes only
						if(debugMode)
						{
							printKeyAttributes(session,dekKeyAlias,dekKeyHandle);
						}
						System.out.println("Trying encryption decryption with dek ");
						EncryptDecrypt(session,dekKeyHandle,encMech,decMech,plainText);
	                }
	                else
	                {
	                	System.out.println("DEK not found in vormetrics");
	                }
	                if(kekKeyAlias != null)
	                {
	                	// find the kek in vault - else create one
	                	
	                	kekKeyHandle = Helper.findKey(session, kekKeyAlias);
	                	if(kekKeyHandle == 0)
	                	{
	                		System.out.println("KEK not found in vormetrics - creating");
	                		kekKeyHandle = Helper.createKey(session, kekKeyAlias, 0, keyLife);
	                		if( kekKeyHandle != 0)
	                		{
	                			System.out.println("KEK created in vormetrics");
	                		}
	                	}
	                	System.out.println("KEK Found in vormetrics. Handle is "+kekKeyHandle);
	                	System.out.println("Trying encryption decryption with kek ");
						EncryptDecrypt(session,kekKeyHandle,encMech,decMech,plainText);
	                }
				}
	        } catch (Exception e) {
                e.printStackTrace();
        }
        finally {
        	if(null != session)
        	{
        		
        		Helper.closeDown(session);
        	}
            System.out.println("End Vormetrics POC.");
        }
	}
	
	private static void EncryptDecrypt(Vpkcs11Session session,long keyHandle, CK_MECHANISM encMech, CK_MECHANISM decMech, String textToOperateOn)
	{
    	byte[] outText = {};
    	byte[] encryptedText;
        byte[] encryptedBytes;
        byte[] decryptedBytes;
        byte[] decryptedData;
        int encryptedDataLen = 0;
        int decryptedDataLen = 0;
        
     	String decryptedTextStr;
		try {
			byte[] plainBytes = textToOperateOn.getBytes();
			int plainBytesLen = plainBytes.length;
			
			
			session.p11.C_EncryptInit(session.sessionHandle, encMech, keyHandle);
			System.out.println("C_EncryptInit success.");
			
			
			encryptedDataLen = session.p11.C_Encrypt (session.sessionHandle,
					plainText.getBytes(), 0, plainText.length(), outText, 0, outText.length);
			
			//encryptedDataLen = session.p11.C_Encrypt(session.sessionHandle, plainBytes, 0, plainBytesLen, outText, 0, 0);
			System.out.println("C_Encrypt success. Encrypted data len = " + encryptedDataLen);

			encryptedText = new byte[encryptedDataLen];
			encryptedDataLen = session.p11.C_Encrypt(session.sessionHandle, plainBytes, 0, plainBytesLen, encryptedText, 0, encryptedDataLen);
			System.out.println("C_Encrypt 2nd call succeed. Encrypted data len = " + encryptedDataLen);

			encryptedBytes = new byte[encryptedDataLen];
			System.arraycopy(encryptedText, 0, encryptedBytes, 0, encryptedDataLen);
			String encryptedTextStr = new String(encryptedBytes, Charset.forName(utfMode));
			System.out.println("Encrypted Text =  " + encryptedTextStr);
			
			// starting decryption
			session.p11.C_DecryptInit(session.sessionHandle, decMech, keyHandle);
			System.out.println("C_DecryptInit success.");

			decryptedDataLen = session.p11.C_Decrypt(session.sessionHandle, encryptedBytes, 0, encryptedDataLen, outText, 0, 0);
			System.out.println("C_Decrypt success. Decrypted data length = " + decryptedDataLen);

			decryptedData = new byte[decryptedDataLen];
			decryptedDataLen = session.p11.C_Decrypt(session.sessionHandle, encryptedBytes, 0, encryptedDataLen, decryptedData, 0, decryptedDataLen);
			System.out.println("C_Decrypt 2nd call succeed. Decrypted data length = " + decryptedDataLen);

			decryptedBytes = new byte[decryptedDataLen];
			System.arraycopy(decryptedData, 0, decryptedBytes, 0, decryptedDataLen);

			decryptedTextStr = new String(decryptedBytes, Charset.forName(utfMode));
			String plainTextStr = new String(plainBytes, Charset.forName(utfMode));
			System.out.println("Decrypted Text = " + decryptedTextStr);
		} catch (PKCS11Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
    private static void printKeyLength(Vpkcs11Session session, long keyHandle) {
    	byte[] keyBytes = null;
    	SecretKey key = null;
    	if (keyHandle != 0)
		{
		    System.out.println("Trying to extract key");
		    
		    try {
				CK_MECHANISM mechanism = new CK_MECHANISM (CKM_AES_CTR, iv);
				//passing in an invalid wrapping key handle
				keyBytes = session.p11.C_WrapKey(session.sessionHandle, mechanism, 0, keyHandle );
				if(null != keyBytes)
				{
					System.out.println("key bytes isnt null");
					key = new SecretKeySpec(keyBytes, 0, keyBytes.length, "AES"); 
					if(key != null)
					{
						System.out.println("Key Algorithm is - "+key.getAlgorithm());
						System.out.println("Key format is - "+key.getFormat());
						System.out.println("Key length is - "+key.getEncoded().length*8);
					}
				}
			} catch (PKCS11Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    System.out.println("END - Trying to extract key");
		}
		
	}

	private static void printKeyAttributes(Vpkcs11Session session, String keyAlias, long keyHandle) {
    	System.out.println("-----> Getting custom attributes for key [" + keyAlias + "]...");
        CK_ATTRIBUTE[] getAttributes = new CK_ATTRIBUTE[]{
                new CK_ATTRIBUTE(CKA_KEY_TYPE)
                , new CK_ATTRIBUTE(CKA_LABEL)
                , new CK_ATTRIBUTE(CKA_KEY_TYPE)
                , new CK_ATTRIBUTE(CKA_VALUE_LEN)
                , new CK_ATTRIBUTE(CKA_TOKEN)
                , new CK_ATTRIBUTE(CKA_ENCRYPT)
                , new CK_ATTRIBUTE(CKA_DECRYPT)
                , new CK_ATTRIBUTE(CKA_SIGN)
                , new CK_ATTRIBUTE(CKA_VERIFY)
                , new CK_ATTRIBUTE(CKA_WRAP)
                , new CK_ATTRIBUTE(CKA_UNWRAP)
                , new CK_ATTRIBUTE(CKA_EXTRACTABLE)
                , new CK_ATTRIBUTE(CKA_ALWAYS_SENSITIVE)
                , new CK_ATTRIBUTE(CKA_NEVER_EXTRACTABLE)
                , new CK_ATTRIBUTE(CKA_SENSITIVE)
                , new CK_ATTRIBUTE(CKA_THALES_DATE_KEY_DEACTIVATION)
                , new CK_ATTRIBUTE(CKA_THALES_DATE_OBJECT_CREATE)
                , new CK_ATTRIBUTE(Helper.CKA_THALES_DATE_OBJECT_CREATE_EL)
                , new CK_ATTRIBUTE(Helper.CKA_THALES_DATE_KEY_DEACTIVATION_EL)
        };
        try {
			session.p11.C_GetAttributeValue(session.sessionHandle, keyHandle, getAttributes);
		} catch (PKCS11Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Helper.printAttributes(getAttributes);
		
	}

	static void usage()
    {
        System.out.println("usage: java com.tjx.test.VormetricsPoC -p pin -dek dekKeyAlias [-kek masterKeyAlias -debug true]");
        System.exit (1);
    }

    public static boolean isKeySymmetric(long keyHandle)
    {
        return (keyHandle & 0xF0000000L) == 0x20000000L;
    }
    
}
