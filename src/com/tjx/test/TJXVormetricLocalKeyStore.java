package com.tjx.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.SecretKey;


public class TJXVormetricLocalKeyStore {

	private String keyStoreLocation = "";
	private String keyStorePwd = "";
	private String keyStoreType = "";
	public Key retrieveKey(String pAlias) throws Exception {
		
		Key key = null;
		if (null != getKeyStoreLocation() && null != getKeyStorePwd() && null != pAlias) {

			try {
				char[] pwdArray = getKeyStorePwd().toCharArray();

				KeyStore ks = KeyStore.getInstance(getKeyStoreType());
				ks.load(new FileInputStream(getKeyStoreLocation()), pwdArray);
				key = (SecretKey) ks.getKey(pAlias, "".toCharArray());
				System.out.println("Loaded key - "+pAlias+" algo is "+key.getAlgorithm());

			} catch (KeyStoreException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (CertificateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (UnrecoverableKeyException e) {
				e.printStackTrace();
			} catch(Exception e) {
				e.printStackTrace();
			}

		} else {
			System.out.println("Not enough params to perform operation");
		}
		return key;
	}
	public String getKeyStoreLocation() {
		return keyStoreLocation;
	}
	public void setKeyStoreLocation(String keyStoreLocation) {
		this.keyStoreLocation = keyStoreLocation;
	}
	public String getKeyStorePwd() {
		return keyStorePwd;
	}
	public void setKeyStorePwd(String keyStorePwd) {
		this.keyStorePwd = keyStorePwd;
	}
	public String getKeyStoreType() {
		return keyStoreType;
	}
	public void setKeyStoreType(String keyStoreType) {
		this.keyStoreType = keyStoreType;
	}
	
}
