package com.tjx.test;

import java.security.Key;

public class TestMain {
	private TJXVormetricLocalKeyStore vormetricLocalKeyStore = null;
	private Boolean useLocalKeyStore = Boolean.TRUE;
	public static void main(String[] args) throws Exception {
		System.out.println("START ");
		TJXVormetricLocalKeyStore localKeyStore = new TJXVormetricLocalKeyStore();
		TestMain main = new TestMain();
		main.setVormetricLocalKeyStore(localKeyStore);
		main.retrieveKey("QA-PG-1");
		System.out.println("END ");
	}

	private static String getString() {
		String str = null;
		try {
			//str = "first string";
			throw new Exception("Error");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return str;
	}
	public Key retrieveKey(String pAlias) throws Exception {
		try {
			
				System.out.println("Going to fetch the key with alias -"+pAlias);
			Key key = null;
			if(isUseLocalKeyStore())
			{
				System.out.println("Going to fetch the key with alias from local key store - "+pAlias);
				key = getVormetricsLocalKeyStore().retrieveKey(pAlias);
				if(key == null)
				{
					System.out.println("The local vormetrics keystore gave error - trying to load from vormetrics vault itself");
				}
				else {
					System.out.println("The local vormetrics keystore contains the key and loaded successfully - "+pAlias);
				}
			}
			else
			{
				System.out.println("Ttrying to load from vormetrics vault itself");
			}
			if(key == null)
			{
				System.out.println("Key got from Vormetrics is null");
				throw new Exception("Key is null");
			}
			return key;
		} catch(Exception e) {
			System.out.println("Error getting the key with alias "+e);
			throw new Exception(e);
		}
	}

	public TJXVormetricLocalKeyStore getVormetricsLocalKeyStore() {
		return vormetricLocalKeyStore;
	}

	public void setVormetricLocalKeyStore(TJXVormetricLocalKeyStore vormetricLocalKeyStore) {
		this.vormetricLocalKeyStore = vormetricLocalKeyStore;
	}

	public Boolean isUseLocalKeyStore() {
		return useLocalKeyStore;
	}

	public void setUseLocalKeyStore(Boolean useLocalKeyStore) {
		this.useLocalKeyStore = useLocalKeyStore;
	}

}
